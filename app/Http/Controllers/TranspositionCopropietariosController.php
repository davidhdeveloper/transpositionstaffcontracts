<?php

namespace App\Http\Controllers;

use App\Models\AmountContractOwner;
use App\Models\Contract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class TranspositionCopropietariosController extends Controller
{
    public function import(Request $request)
    {
        ini_set('max_execution_time', 0);

        $file = $request->file('importFile');

        if (!$file) {
            return response('Archivo no recibido', 400);
        }

        $file_tmp = $file->getRealPath();
        $rows = file($file_tmp);

        foreach ($rows as $index => $row) {
            $fields = explode(';', $row);

            try {
                $contract = Contract::where('contractCode', $fields[0])->first();
                if ($contract) {
                    # Cantidad de propietarios asignados al contrato
                    $amountOwnerContract = AmountContractOwner::where('contractCode', $contract->contractCode)->first();
                    $cantOwner = $amountOwnerContract->amountOwner;
                    # Agregar filas dinámicas de copropietarios (transposición)
                    $newFieldContract = new TranspositionCopropietariosController();
                    $fieldsCopro = $newFieldContract->newFieldsContract($cantOwner + 1);
                    
                    # Llenar campos
                    # Elimina la posición 0 del array
                    array_shift($fields);

                    foreach ($fields as $fieldIndex => $field) {
                        // Comprobar si el índice existe en el array de campos antes de asignar
                        if (isset($fieldsCopro[$fieldIndex])) {
                            $contract->{$fieldsCopro[$fieldIndex]} = $field;
                        }
                    }
                    $amountOwnerContract->totalPercentage += floatval($fields[2]);
                    $contract->save();
                    $amountOwnerContract->save();
                    $amountOwnerContract->increment('amountOwner');
                }
            } catch (\Exception $e) {
                return response($e->getMessage());
            }
        }

        return response('Carga de copropietarios finalizada correctamente.', 200);
    }

    public function newFieldsContract($cantOwner){
        
        $newFields = [
            "propietary".$cantOwner."_identification_ownerIdentificationType",
            "propietary".$cantOwner."_identification_ownerIdentification",
            "propietary".$cantOwner."_payment_paymentShare",
            "propietary".$cantOwner."_payment_bankCode",
            "propietary".$cantOwner."_payment_accountTypeCode",
            "propietary".$cantOwner."_payment_accountNumber",
            "propietary".$cantOwner."_payment_allowsCheck",
            "propietary".$cantOwner."_payment_allowsEFTTransfer",
            "propietary".$cantOwner."_info_allowsMail",
            "propietary".$cantOwner."_payment_fullname",
            "propietary".$cantOwner."_payment_identificationType",
            "propietary".$cantOwner."_payment_identification"
        ];

        try {
            Schema::table('contracts', function($table) use ($newFields){
                foreach ($newFields as $newField) {
                    if (!Schema::hasColumn('contracts', $newField)) {
                        $table->string($newField)->nullable();
                    }
                }
            });
    
            return $newFields;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
