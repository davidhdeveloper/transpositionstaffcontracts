<?php

namespace App\Http\Controllers;

use App\Models\AmountContractOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class HeadersContractController extends Controller
{
    public function import(Request $request)
    {
        $file = $request->file('importFile');

        if ($file) {
            $file_tmp = $file->getRealPath();
            $rows = file($file_tmp);

            Schema::dropIfExists('contracts');
            Schema::create('contracts', function ($table) use ($rows) {
                $table->increments('id');
                foreach ($rows as $index => $row) {
                    $fields = explode(";", $row);
                    foreach ($fields as $field) {
                        $table->string($field)->nullable();
                    }
                }
                $table->timestamps();
            });

            AmountContractOwner::truncate();

            return response()->json('Entidad creada exitosamente', 200);
        }

        return response('Archivo no recibido', 400);
    }
}
