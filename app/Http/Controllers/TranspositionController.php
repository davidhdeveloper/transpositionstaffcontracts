<?php

namespace App\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class TranspositionController extends Controller
{
    public function importData(Request $request){
        try {

            $nameTable = $request->nameTable;

            $importClass = [
                'headers_contract' => HeadersContractController::class,
                'contracts' => ContractsController::class,
                'transposition_copropietarios' => TranspositionCopropietariosController::class,
                'transposition_coarrendatarios' => TranspositionCoarrendatariosController::class,
                'verification_nit' => VerificationNitController::class
            ];

            if (!array_key_exists($nameTable, $importClass)) {
                return response('Importación no realizada, revisa los parametros de entrada', 400);
            }
    
            $action = new $importClass[$nameTable];
            
            $loadData = $action->import($request);
    
            return response('Importación realizada en ' . $nameTable . ' ' . $loadData, 200);

        } catch (\Throwable $th) {
            return response()->json($th, 404);
        }
    }
}
