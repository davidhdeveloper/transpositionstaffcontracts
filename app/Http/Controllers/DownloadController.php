<?php

namespace App\Http\Controllers;

use App\Exports\ContractsExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DownloadController extends Controller
{
    public function exportContract(){
        try {
            return Excel::download(new ContractsExport, 'contracts-with-transposition.xlsx', null, ['headers' => true]);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
