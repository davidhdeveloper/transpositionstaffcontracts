<?php

namespace App\Http\Controllers;

use App\Models\AmountContractOwner;
use App\Models\Contract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

use function Laravel\Prompts\text;

class ContractsController extends Controller
{
    public function import(Request $request)
    {
        ini_set('max_execution_time', 0);

        $file = $request->file('importFile');

        if ($file) {
            $file_tmp = $file->getRealPath();
            $rows = file($file_tmp);

            $columns = Schema::getColumnListing('contracts');
            
            foreach ($rows as $index => $row) {
                $fields = explode(";", $row);
                
                if (count($fields) != count($columns) - 3) {
                    // El número de campos no coincide con el número de columnas
                    // Suponiendo que las columnas 'id', 'created_at' y 'updated_at' se excluyen
                    continue;
                }
                
                try {
                    $contract = new Contract();
        
                    foreach ($columns as $columnIndex => $column) {
                        if ($column == 'id' || $column == 'created_at' || $column == 'updated_at') {
                            continue;
                        }
                        $contract->$column = $fields[$columnIndex - 1]; // Restamos 1 para ajustar el índice

                        if ($column == 'contractCode') {
                            $incrementAmountContract = new AmountContractOwner;
                            $incrementAmountContract->contractCode = $fields[$columnIndex - 1];
                            $incrementAmountContract->amountOwner = 1;
                            $incrementAmountContract->totalPercentage = floatval($fields[18]);
                            $incrementAmountContract->save();
                        }
                    }
            
                    $contract->save();

                } catch (\Exception $e) {
                    return response($e->getMessage());
                }
            }
            return response()->json('Registros creados exitosamente', 200);
        }

        return response('Archivo no recibido', 400);
    }
}
