<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class VerificationNitController extends Controller
{
    public function import(Request $request)
    {
        ini_set('max_execution_time', 0);

        $file = $request->file('importFile');

        if (!$file) {
            return response('Archivo no recibido', 400);
        }

        $file_tmp = $file->getRealPath();
        $rows = file($file_tmp);

        $generateCodeNit = new VerificationNitController();

        foreach ($rows as $index => $row) {
            $fields = explode(';', $row);
            $fieldNit = str_replace(array("\n", "\r"), '', $fields[1]);
            try {
                $typesDocument = Contract::where($fields[0], 'Nit')->get();
                if ($typesDocument) {
                    foreach ($typesDocument as $contract) {
                        if (Schema::hasColumn('contracts', $fieldNit)) {
                            if (strpos($contract->$fieldNit, '-') == false) {
                                $firstNineDigits = substr($contract->$fieldNit, 0, 9);
                                $verificationCode = $generateCodeNit->addVerificationCodeNit((int)$firstNineDigits);
                                $contract->$fieldNit = $firstNineDigits.'-'.$verificationCode;
                                $contract->save();
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                return response($e->getMessage());
            }
        }

        return response('Código de verificación agregado correctamente.', 200);
    }

    public function addVerificationCodeNit($nit){
        if (! is_numeric($nit)) {
            return false;
        }
     
        $arr = array(1 => 3, 4 => 17, 7 => 29, 10 => 43, 13 => 59, 2 => 7, 5 => 19, 
        8 => 37, 11 => 47, 14 => 67, 3 => 13, 6 => 23, 9 => 41, 12 => 53, 15 => 71);
        $x = 0;
        $y = 0;
        $z = strlen($nit);
        $dv = '';
        
        for ($i=0; $i<$z; $i++) {
            $y = substr($nit, $i, 1);
            $x += ($y*$arr[$z-$i]);
        }
        
        $y = $x%11;
        
        if ($y > 1) {
            $dv = 11-$y;
            return $dv;
        } else {
            $dv = $y;
            return $dv;
        }
    }
}
