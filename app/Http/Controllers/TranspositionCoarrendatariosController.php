<?php

namespace App\Http\Controllers;

use App\Models\AmountContractOwner;
use App\Models\Contract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class TranspositionCoarrendatariosController extends Controller
{
    public function import(Request $request)
    {
        ini_set('max_execution_time', 0);

        $file = $request->file('importFile');

        if (!$file) {
            return response('Archivo no recibido', 400);
        }

        $file_tmp = $file->getRealPath();
        $rows = file($file_tmp);

        foreach ($rows as $index => $row) {
            $fields = explode(';', $row);

            try {
                $contract = Contract::where('contractCode', $fields[0])->first();
                if ($contract) {
                    # Cantidad de arrendatarios asignados al contrato
                    $amountTenantContract = AmountContractOwner::where('contractCode', $contract->contractCode)->first();
                    $cantTenant = $amountTenantContract->amountTenant;
                    # Agregar filas dinámicas de coarrendatarios (transposición)
                    $newFieldContract = new TranspositionCoarrendatariosController();
                    $fieldsCoarre = $newFieldContract->newFieldsContract($cantTenant + 1);
                    
                    # Llenar campos
                    # Elimina la posición 0 del array
                    array_shift($fields);

                    foreach ($fields as $fieldIndex => $field) {
                        // Comprobar si el índice existe en el array de campos antes de asignar
                        if (isset($fieldsCoarre[$fieldIndex])) {
                            $contract->{$fieldsCoarre[$fieldIndex]} = $field;
                        }
                    }
                    $contract->save();
                    $amountTenantContract->save();
                    $amountTenantContract->increment('amountTenant');
                }
            } catch (\Exception $e) {
                return response($e->getMessage());
            }
        }

        return response('Carga de coarrendatarios finalizada correctamente.', 200);
    }

    public function newFieldsContract($cantTenant){
        
        $newFields = [
            "cosigner".$cantTenant."_identification_identificationType",
            "cosigner".$cantTenant."_identification_identification",
        ];

        try {
            Schema::table('contracts', function($table) use ($newFields){
                foreach ($newFields as $newField) {
                    if (!Schema::hasColumn('contracts', $newField)) {
                        $table->string($newField)->nullable();
                    }
                }
            });
    
            return $newFields;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
