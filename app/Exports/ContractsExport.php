<?php

namespace App\Exports;

use App\Models\AmountContractOwner;
use App\Models\Contract;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class ContractsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;
    
    public function collection()
    {
        return Contract::all();
    }
}
