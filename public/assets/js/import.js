document.addEventListener('DOMContentLoaded', (event) => {
    let btnImport = document.querySelector('#btnImport');

    btnImport.addEventListener('click', importar);


    function importar() {
        let importFile = document.getElementById('importFile');
        let nameTable = document.getElementById('selectTable').value;
        let file = importFile.files[0];
        
        let data = new FormData();
        data.append("importFile", file);
        data.append("nameTable", nameTable);
        console.log('Cargando importación...');

        axios.post(routes.transposition, data)
        .then(function (response) {
            alert(response.data);
            console.log(response.data);
            console.log('Carga correcta');
        })
        .catch(function (error) {
            alert(error.response.data);
            console.log(error.response.data);
            console.log('Carga incorrecta');
        });
    }
});
